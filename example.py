import warnings

import numpy as np
from skimage import io

from src.spectroflat import Analyser, Config, SmileConfig, SensorFlatConfig
from qollib.strings import parse_shape

imgs = [f'tests/resources/dirty_flat_{s}.png' for s in range(4)]
# imgs = [f'report/bbso_flat_{s}.png' for s in range(4)]
cube = np.array([io.imread(img)[:, :, 0] for img in imgs])
report_dir = 'report'
cube[:, 100:105, 300:301] = -0.5

roi = None
# roi = parse_shape('[50:500,100:600]')
roi = parse_shape(f'[2:{cube.shape[1]-2},2:{cube.shape[2]-2}]')

config = Config(roi=roi, sensor_flat_iterations=3)
config.sensor_flat = SensorFlatConfig(spacial_degree=13,
                                      sigma_mask=2,
                                      fit_border=1,
                                      average_column_response_map=True,
                                      ignore_gradient=False,
                                      roi=roi)
config.smile = SmileConfig(line_distance=16,
                           max_dispersion_deg=5,
                           line_prominence=0.1,
                           height_sigma=0.04,
                           smooth=True,
                           emission_spectrum=False,
                           state_aware=False,
                           smile_deg=0,
                           rotation_correction=0,
                           detrend=True,
                           roi=roi)

an = Analyser(cube, config, report_dir)
an.run()

an.offset_map.dump(f'{report_dir}/offset_map.fits')
