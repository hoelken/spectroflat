"""
## Utility package

The `utils` package provides small (predominant static) utilities for recurring
tasks in the `spectroflat` code base. For example spawning threads and processes
or dealing with collections.

Please refer to the description of the individual modules for more details.
"""

from .processing import MP
from .collections import Collections
