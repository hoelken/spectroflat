import warnings

import numpy as np
from skimage import io

from src.spectroflat.analyzer import Analyser
from src.spectroflat.base.config import Config


def test_smile_analysis():
    config = Config.from_dict(
        {'apply_sensor_flat': False, 'smile': {'line_prominence': 0.5, 'smooth': False, 'max_dispersion_deg': 3}}
    )
    an = Analyser(np.array([io.imread('tests/resources/flat_cutout.png')[0:160, 0:90, 0]]), config)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        an.run()

    assert an.gain_table.shape == (1, 160, 90)
    assert an.desmiled.shape == (1, 160, 90)

    assert an.offset_map.header['squashed'] == 'False'
    assert an.offset_map.header['Shape'] == '(1, 160, 90)'
