from copy import copy

import numpy as np

from src.spectroflat.analyzer import Analyser
from src.spectroflat.base.config import Config, SmileConfig
from src.spectroflat.smile.offset_map import OffsetMap

CUBE = np.empty((2, 2, 2))
CONF = Config()


def test_compute_offset(mocker):
    smg = mocker.patch('src.spectroflat.analyzer.SmileMapGenerator').return_value
    smg.run.return_value = smg
    smg.mean.return_value = 1
    smg.std.return_value = 2
    smg.abs_max.return_value = 3
    smg.mean_px_deviation.return_value = 0
    smg.std_px_deviation.return_value = 1
    smg.max_px_deviation.return_value = 2
    an = Analyser(CUBE, CONF)
    an._compute_offsets()

    assert an.offset_map == smg.omap


def test_remove_lines(mocker):
    lr = mocker.patch('src.spectroflat.analyzer.LineRemover').return_value
    lr.run.return_value = lr
    lr.result = np.array([[[2, 2], [2, 4]], [[4, 8], [4, 4]]])

    an = Analyser(CUBE, CONF)
    an._remove_lines()
    np.testing.assert_array_equal(an.gain_table, [[[0.8, 0.8], [0.8, 1.6]], [[0.8, 1.6], [0.8, 0.8]]])
