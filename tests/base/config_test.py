from src.spectroflat.base.config import Config


def test_default():
    assert Config() is not None


def test_roi_from_dict():
    c = Config.from_dict({'roi': '[0:100,200:300]'})
    assert c.roi == (slice(0, 100), slice(200, 300))
