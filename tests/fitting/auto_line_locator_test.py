import numpy as np
from src.spectroflat.fitting import AutoLineLocator


def test_peak_detection():
    img = np.zeros((100, 100))
    img[:, 10] = 10
    img[:, 50] = 15
    img[:, 70] = 10
    al = AutoLineLocator(img, line_distance=15)
    peaks = al.detect_centers()
    assert peaks[0] == 10
    assert peaks[1] == 50
    assert peaks[2] == 70
