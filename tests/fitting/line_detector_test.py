#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for gaussian fits

@author: hoelken
"""

import numpy as np
from qollib.processing.testing import sequentially
from src.spectroflat.fitting import LineDetector

from tests.helper import mocks


def test_line_detector(mocker):
    mocker.patch('scipy.signal.find_peaks', return_value=([5], None))
    mocker.patch('src.spectroflat.fitting.line_detector.simultaneous', new=sequentially)
    mock = mocker.patch('src.spectroflat.fitting.line_detector.LineFit')
    mocked_instance = mock.return_value
    mocked_instance.max_location = 6

    data = np.arange(1000).reshape(20, 50)
    ld = LineDetector(data, anchors=3, line_centers=[17, 34])
    ld.run()

    assert mocked_instance.run.called
    assert ld.lines[0].map == [(6, 17), (6, 34)]


def test_line_detector_error_handling(mocker):
    mocker.patch('scipy.signal.find_peaks', return_value=([5], None))
    mocker.patch('src.spectroflat.fitting.line_detector.simultaneous', new=sequentially)
    mock = mocker.patch('src.spectroflat.fitting.line_detector.LineFit')
    mocked_instance = mock.return_value
    mocked_instance.run.side_effect = RuntimeError("TESTING: 1, 2, 3")

    data = np.arange(10).reshape(2, 5)
    ld = LineDetector(data, anchors=3, line_centers=[17, 34])
    ld.run()

    assert mocked_instance.run.called
