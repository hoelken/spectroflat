#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for line fits

@author: hoelken
"""
import numpy as np
import pytest

from src.spectroflat.fitting.line_fit import LineFit, gaussian, lorentzian


def test_no_data():
    with pytest.raises(RuntimeError):
        LineFit([], []).run()


def test_no_peak():
    xes = range(20)
    yes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

    line_fit = LineFit(xes, yes)
    with pytest.raises(RuntimeError):
        line_fit.run()


def test_gauss_fit():
    amp1 = 100
    width1 = 15
    cen1 = 52

    xes = np.array(range(70))
    y_array_lorentz = gaussian(xes, amp1, cen1, width1)
    y_noise_p = np.random.ranf(70) * 0.07
    y_noise_m = np.random.ranf(70) * 0.07
    yes = y_array_lorentz + y_noise_p - y_noise_m

    lf = LineFit(xes, yes, error_threshold=5)
    lf.run()

    assert lf.max_location > 51
    assert lf.max_location < 53


def test_lorentzian():
    amp1 = 100
    width1 = 23
    cen1 = 40

    xes = np.array(range(70))
    y_array_lorentz = lorentzian(xes, amp1, cen1, width1)
    y_noise_p = np.random.ranf(70) * 0.07
    y_noise_m = np.random.ranf(70) * 0.07
    yes = y_array_lorentz + y_noise_p - y_noise_m

    lf = LineFit(xes, yes, error_threshold=5)
    lf.run()

    assert lf.max_location > 39.5
    assert lf.max_location < 40.5
