#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for line dataclass

@author: hoelken
"""

from src.spectroflat.fitting import Line


def test_line_area():
    # Check that the area does not overlap the borders of our rows
    assert Line(30, 100).area(0) == range(5, 55)
    assert Line(5, 100).area(0) == range(0, 30)
    assert Line(20, 40).area(0) == range(0, 40)


def test_line_area_positive_inclination():
    # Check that the area does not overlap the borders of our rows
    line = Line(center=50, max_length=100, rot_anker=25, rotation=2)
    assert line.area(0) == range(26, 76)
    assert line.area(25) == range(25, 75)
    assert line.area(50) == range(24, 74)


def test_line_area_negative_inclination():
    # Check that the area does not overlap the borders of our rows
    line = Line(center=50, max_length=100, rot_anker=25, rotation=-0.78)
    assert line.area(0) == range(25, 75)
    assert line.area(25) == range(25, 75)
    assert line.area(50) == range(25, 75)
    assert line.area(99) == range(26, 76)


def test_gaps_x():
    line = Line(150, 500)
    line.add((10, 12))
    line.add((15, 12))
    line.add((35, 12))
    line.add((29, 12))
    line.add((87, 12))
    assert [10, 5, 14, 6, 52] == line.gaps(0)


def test_gaps_y():
    line = Line(150, 500)
    line.add((5, 10))
    line.add((5, 15))
    line.add((5, 35))
    line.add((5, 29))
    line.add((5, 87))
    assert [10, 5, 14, 6, 52] == line.gaps(1)
