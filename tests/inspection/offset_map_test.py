from unittest.mock import MagicMock

import numpy as np

from src.spectroflat.inspection.offset_map import plt_map
from src.spectroflat.smile import OffsetMap


def test_heatmap(mocker):
    offsets = np.arange(40000).reshape((4, 100, 100))
    smap = OffsetMap({'squashed': True})
    smap.map = offsets
    smap.error = offsets

    fig = MagicMock()
    axs = MagicMock()
    plt = mocker.patch('src.spectroflat.inspection.offset_map.plt')
    mocker.patch('src.spectroflat.inspection.offset_map.cbar')
    plt.subplots.return_value = (fig, axs)
    plt_map(smap, rows=(10, 33, 50, 90))

    assert plt.show.called
