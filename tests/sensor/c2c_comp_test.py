import numpy as np

from src.spectroflat.sensor.c2c_comp import extract_c2c_comp


def test_simple_pattern():
    shape = (15, 20)
    img, ratio = _create_patter(shape)
    pattern = extract_c2c_comp(img, shape)

    assert pattern.shape == (1, shape[0], shape[1])
    assert pattern[0, 0, 0] == 1
    np.testing.assert_almost_equal(pattern[0, 0, 1], ratio)


def test_other_size():
    shape = (23, 37)
    img, ratio = _create_patter((10, 12))
    pattern = extract_c2c_comp(img, shape)

    assert pattern.shape == (1, shape[0], shape[1])
    assert pattern[0, 0, 0] == 1
    np.testing.assert_almost_equal(pattern[0, 0, 1], ratio)


def _create_patter(shape: tuple) -> tuple:
    img = np.ones(shape)
    ratio = np.random.random()
    for c in range(0, shape[1]-1, 2):
        img[:, c] = ratio
    return np.array([img]), ratio
