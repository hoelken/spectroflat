import numpy as np

from src.spectroflat.sensor.smooth_model import SmoothModel, SensorFlatConfig


def test_default_configuration():
    config = SensorFlatConfig()
    model = SmoothModel(np.empty((4, 5)), config)
    model._configure()

    assert model._config.total_rows == 4
    assert model._config.total_cols == 5
    assert model._rows == range(0, 4)
    assert model._cols == range(0, 5)


def test_roi_configuration():
    config = SensorFlatConfig(roi=(slice(1, 45), slice(1, 40)))
    model = SmoothModel(np.empty((50, 50)), config)
    model._configure()

    assert model._config.total_rows == 50
    assert model._config.total_cols == 50
    np.testing.assert_array_equal(model._rows, np.arange(1, 45))
    assert model._cols == range(1, 40)


def test_creation():
    img = np.ones((50, 50))
    config = SensorFlatConfig(spacial_degree=1, fit_border=5)
    model = SmoothModel(img + 3, config)
    model.create()

    np.testing.assert_allclose(model.img, img)


def test_creation_with_roi():
    img = np.ones((50, 50))
    config = SensorFlatConfig(spacial_degree=1, roi=(slice(2, 40), slice(1, 45)), fit_border=2)
    model = SmoothModel(img + 3, config)
    model.create()

    np.testing.assert_allclose(model.img, img)
