from scipy import ndimage
from skimage import io

from src.spectroflat.shift.img_rotation import RotationAnalysis, RotationCorrection

IMG = io.imread('tests/resources/flat_cutout.png')[:, :, 0]


def test_no_rotation():
    ra = RotationAnalysis(IMG).run()
    assert ra.angle == 0


def test_with_rotation():
    rotated = ndimage.rotate(IMG, 3)
    ra = RotationAnalysis(rotated).run()
    assert -3.2 < ra.angle < -2.8


def test_vertical_rotation():
    assert RotationAnalysis.detect_vertical_rotation(IMG) == 0


def test_horizontal_rotation():
    assert RotationAnalysis.detect_vertical_rotation(IMG) == 0


def test_bicubic_rotation():
    # We completely rely on the skikit algorithm and do not want to test this.
    # Just test that this is running without errors and cuts out the correct shape
    rotated = RotationCorrection(IMG, 0.75).bicubic(cut_shape=True)
    assert rotated.shape[0] == IMG.shape[0] - 4
    assert rotated.shape[1] == IMG.shape[1] - 4
