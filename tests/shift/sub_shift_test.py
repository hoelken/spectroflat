#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for sub shift module

@author: hoelken
"""

import numpy as np

from src.spectroflat.shift.sub_shift import Shifter


def test_d1shift_fullpx():
    data = np.arange(9)
    expected = [2.8, 4, 4.7, 4.6, 3.8, 2.4, 0, 0.5, 1.5]
    result = Shifter.d1shift(data, 3.)
    np.testing.assert_allclose(expected, result, rtol=0.1, atol=0.1)


def test_d1shift_subpx():
    data = np.arange(9)
    expected = [4.5, 4.8, 4.0, 2.9, 0.4, 0.2, 1.3, 2.4, 3.8]
    result = Shifter.d1shift(data, 4.75)
    np.testing.assert_allclose(expected, result, rtol=0.1, atol=0.1)


def test_d2shift_fullpx():
    data = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]])
    expected = [[4, 2.7, 1.6], [3.8, 2.4, 1.8], [0.5, 0.6, 0]]
    result = Shifter.d2shift(data, 1.)
    np.testing.assert_allclose(expected, result, rtol=0.1, atol=0.1)


def test_d2shift_subpx():
    data = np.array([[0, 1, 2], [3, 4, 5], [6, 7, 8]])
    expected = [[1, 1.9, 0.7], [3.8, 4.7, 2.0], [1.1, 1.5, 0.6]]
    result = Shifter.d2shift(data, .5)
    np.testing.assert_allclose(expected, result, rtol=0.1, atol=0.1)
