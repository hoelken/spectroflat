import numpy as np
from src.spectroflat.smile.smoothing import LineRemover


def test_line_remover():
    """
    Generate an array with gradient, horizontal and vertical features.
    """
    v_grad = [0.0005 * i**3 - 0.0002 * i**2 for i in range(-25, 25)]
    row = np.ones(40)

    # add vertical lines
    row[5] = 5
    row[15] = 0.5
    row[16] = 0.7
    row[17] = 0.9
    row[19] = 1.4
    row[20] = 2.1
    row[21] = 1.2
    row[31] = 7.5
    row[32] = 6
    row[32] = 9

    # add horizontal features
    img = [row + v_grad[i] for i in range(50)]
    img[11] = img[11] * 1.5
    img[12] = img[12] * 1.7
    img[13] = img[13] * 2.1
    img[14] = img[14] * 3.3
    img[15] = img[15] * 1.4
    img[16] = img[16] * 0.7

    img[42][9] = 5
    img[23][5] = 3
    img[19][19] = 30
    img[6][6] = -20

    img = np.array([img])

    lr = LineRemover(img).run()
    h_mean = lr.result[0].mean(axis=0)
    np.testing.assert_allclose(h_mean - h_mean.mean(), 0, atol=2e-14)
