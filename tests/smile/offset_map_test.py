#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for smile_map module

@author: hoelken
"""

import numpy as np
from scipy.interpolate import splev, BSpline

from src.spectroflat.smile import OffsetMap


def test_map_header():
    smap = OffsetMap({'Foo': 'Test'})
    assert smap.header['Foo'] == 'Test'
    assert smap.header['squashed'] == 'False'


def test_smap_mean_offset():
    header = {
        'Shape': (slice(0, 2), slice(0, 10)),
        'DIMS': ['row', 'lambda']
    }
    smap = OffsetMap(header)
    smap.map = np.array([np.ones((2, 10)), np.zeros((2, 10)), np.arange(20).reshape((2, 10))])
    smap.error = np.array([np.ones((2, 10)), np.ones((2, 10)), np.ones((2, 10))])

    assert smap.map.shape == (3, 2, 10)
    assert smap.error.shape == (3, 2, 10)
    smap.squash()
    assert smap.map.shape == (3, 2, 10)
    assert smap.is_squashed()

    assert smap.map[0, 0, 2] == 1
    assert smap.map[0, 1, 1] == 4


def test_smap_same_offset():
    header = {
        'Shape': (slice(0, 2), slice(0, 10)),
        'DIMS': ['row', 'lambda']
    }
    smap = OffsetMap(header)
    smap.map = np.array([np.ones((2, 10)), np.zeros((2, 10)), np.arange(20).reshape((2, 10))])
    smap.error = np.array([np.ones((2, 10)), np.ones((2, 10)), np.ones((2, 10))])

    assert smap.map.shape == (3, 2, 10)
    assert smap.error.shape == (3, 2, 10)
    smap.enforce_same_offsets_on_all_states()
    assert smap.map.shape == (3, 2, 10)
    assert smap.error.shape == (3, 2, 10)
    assert not smap.is_squashed()

    assert smap.map[0, 0, 2] == 1
    assert smap.map[0, 1, 1] == 4
    assert smap.map[1, 1, 1] == 4
    assert smap.map[2, 1, 1] == 4
