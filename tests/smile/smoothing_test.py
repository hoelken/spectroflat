import numpy as np
from src.spectroflat.smile.smoothing import ResidualsRemover, GlobalSmudger


def test_smooth_flat():
    rr = ResidualsRemover(np.ones([1000, 100])).run()
    assert (1000, 100) == rr.img.shape


def test_smudging():
    img = np.ones((5, 6))
    smudger = GlobalSmudger(img, deg=1)
    smudger.run()

    np.testing.assert_allclose(smudger.gain, img)
