#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for collections module

@author: hoelken
"""

import pytest
import numpy as np
from src.spectroflat.utils import Collections


def test_bin_axis_1d():
    data = np.array([0, 2, 0, 2, 0, 2, 0, 2, 0])

    result = Collections.bin_axis(data, 2)
    assert len(result) == 4
    np.testing.assert_allclose(result, [1, 1, 1, 1])

    result = Collections.bin_axis(data, 3)
    assert len(result) == 3
    np.testing.assert_allclose(result, [0.666667, 1.333333, 0.666667], rtol=1e-5)

    result = Collections.bin_axis(data, 4)
    assert len(result) == 2
    np.testing.assert_allclose(result, [1, 1])

    result = Collections.bin_axis(data, -1)
    assert len(result) == 1
    np.testing.assert_allclose(result, [0.88888889])


def test_bin_axis_2d():
    data = np.array([[0, 2, 0, 2], [4, 0, 4, 0]])

    result = Collections.bin_axis(data, 2, axis=1)
    assert result.shape == (2, 2)
    np.testing.assert_allclose(result, [[1, 1], [2, 2]])

    result = Collections.bin_axis(data, 2, axis=0)
    assert result.shape == (1, 4)
    np.testing.assert_allclose(result[0], [2, 1, 2, 1])


def test_bin_axis_other_method():
    data = np.array([0, 1, 2, 3, 4, 5, 7, 8, 9])

    result = Collections.bin_axis(data, 2, method=np.min)
    assert len(result) == 4
    np.testing.assert_allclose(result, [0, 2, 4, 7])

    result = Collections.bin_axis(data, -1, method=np.max)
    assert len(result) == 1
    np.testing.assert_allclose(result, [9])


def test_bin():
    data = np.array([[0, 2, 0, 2], [4, 0, 4, 0]])

    result = Collections.bin(data, [1, 2])
    assert result.shape == (2, 2)
    np.testing.assert_allclose(result, [[1, 1], [2, 2]])

    result = Collections.bin(data, [2, 1])
    assert result.shape == (1, 4)
    np.testing.assert_allclose(result[0], [2, 1, 2, 1])

    result = Collections.bin(data, [2, 2])
    assert result.shape == (1, 2)
    np.testing.assert_allclose(result[0], [1.5, 1.5])

    result = Collections.bin(data, [1, -1])
    assert result.shape == (1, 2)
    np.testing.assert_allclose(result[0], [1, 2])


def test_bin_other_method():
    data = np.array([[0, 1, 2, 3], [4, 5, 6, 7], [8, 9, 10, 11]])

    result = Collections.bin(data, [1, -1], method=np.max)
    assert result.shape == (1, 3)
    np.testing.assert_allclose(result[0], [3, 7, 11])

    result = Collections.bin(data, [2, 1], method=np.min)
    assert result.shape == (1, 4)
    np.testing.assert_allclose(result[0], [0, 1, 2, 3])


def test_bin_border_cases():
    data = np.array([[1, 2, 3, 4], [5, 6, 7, None]])

    # if only 1 is given as binning, do not care about shapes and just do nothing
    result = Collections.bin(data, [1, ])
    np.testing.assert_array_equal(result, data)
    result = Collections.bin(data, [1, 1, 1])
    np.testing.assert_array_equal(result, data)

    # (Not) dealing with None
    with pytest.raises(TypeError):
        Collections.bin(data, [1, 2])

    # Shape checks
    with pytest.raises(Exception):
        Collections.bin(data, [2, ])
    with pytest.raises(Exception):
        Collections.bin(data, [2, 3, 4])


def test_sigma_remove():
    np.testing.assert_array_equal([1, 1, 1], Collections.replace_sigma_outliers(np.array([1, 1, 1])))
    np.testing.assert_array_equal([1, 2, 1], Collections.replace_sigma_outliers(np.array([1, 4, 1]), 1))
