from src.spectroflat.utils.ffing import *


def test_splitting_dust_flat():
    dust = np.ones((2, 5, 100))
    dust[0, 2] = dust[0, 2] + 0.5
    dust[0, 2, 3] = 2
    dust[0, 1, 4] = 1.3
    dust[0, 4, 1] = 0.7

    sensor, slit = split_sensor_slit_flat(dust)
    assert sensor.shape == (2, 5, 100)
    assert slit.shape == (2, 5, 100)

    for r in range(5):
        expectation = 1.5 if r == 2 else 1
        assert np.allclose(slit[0, r], expectation, atol=0.1), f"Mismatch in slit flat row {r}"

    np.testing.assert_almost_equal(sensor[0, 2, 3], 1.32, decimal=2)
    np.testing.assert_almost_equal(sensor[0, 1, 4], 1.29, decimal=2)
    np.testing.assert_almost_equal(sensor[0, 4, 1], 0.70, decimal=2)
