# Test cases for polynomial helper methods
import numpy as np
from src.spectroflat.utils.polynomial import min_value, max_value, abs_max_value


XES = range(-3, 4)


def test_minimum():
    fit = np.poly1d(np.polyfit(XES, _no_max(np.array(XES)), 2))
    np.testing.assert_almost_equal(min_value(fit), -1)
    np.testing.assert_almost_equal(min_value(fit, interval=(-4, -1)), 0)


def test_minimum_only_local():
    fit = np.poly1d(np.polyfit(XES, _no_min(np.array(XES)), 2))
    assert min_value(fit) is None
    np.testing.assert_almost_equal(min_value(fit, interval=(-1, 0)), 0)


def test_maximum():
    fit = np.poly1d(np.polyfit(XES, _no_min(np.array(XES)), 2))
    np.testing.assert_almost_equal(max_value(fit), 1)
    np.testing.assert_almost_equal(max_value(fit, interval=(-4, -1)), 0)


def test_maximum_only_local():
    fit = np.poly1d(np.polyfit(XES, _no_max(np.array(XES)), 2))
    assert max_value(fit) is None
    np.testing.assert_almost_equal(max_value(fit, interval=(-1, 0)), 0)


def test_abs_max_value():
    fit = np.poly1d(np.polyfit(XES, _no_min(np.array(XES)), 2))
    np.testing.assert_almost_equal(abs_max_value(fit), 0)
    np.testing.assert_almost_equal(abs_max_value(fit, interval=(-4, -2)), 15)


def _no_max(x):
    return x**2 - 1


def _no_min(x):
    return (x**2 - 1) * -1
