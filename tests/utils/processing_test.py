#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Test cases for multiprocessing module

@author: hoelken
"""

import pytest
import time

from src.spectroflat.utils import MP


def test_simultaneous_result():
    result = MP.simultaneous(divide_by, [1, 2, 4])
    result = sorted(result)
    assert result[2] == 1.0
    assert result[1] == 0.5
    assert result[0] == 0.25

    result = MP.simultaneous(divide_by, [1])
    assert len(result) == 1


def test_simultaneous_exception():
    with pytest.raises(ZeroDivisionError):
        MP.simultaneous(divide_by, [1, 2, 3, 0, 5, 6, 0])


def test_simultaneous_silent_exception():
    # In this case no exception is raised
    # The results from the successful threads are preserved
    result = MP.simultaneous(divide_by, [1, 2, 3, 0, 5, 6, 0], raise_exception=False)
    assert len(result) == 5


def divide_by(num):
    time.sleep(0.05)
    return 1 / num
